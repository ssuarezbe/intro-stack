# Intro-Stack


1. [Como configurar el ambiente de desarrollo local](Configurando_el_ambiente.md)
    1. Python 3.5
    1. Instalar virtualenv
    1. Instalar virtualenvwrapper

1. [Iniciando con Falcon](Iniciando_con_Falcon.md)
    1. Instalar Falcon
    1. Crear un REST endpoint
    1. Instalando un wsgi (gunicorn)
    1. Poniendo en marcha el proyecto Falcon en un ambiente local con gunicorn
    1. Agregando middleware para autenticacion

1. [Iniciando con grpc.io](Iniciando_con_grpc.md)
    1. Emulando una BD mock
    1. Desplegando el RPC

1. Integrando Falcon con RPC
    1. Integrar el REST endpoint con el RPC de una BD emulada
    1. Instalando SQLite
    1. Integrando SQL lite con grpc.io 
    1. Integrando el nuevo RPC para SQLite con el REST endpoint Falcon

