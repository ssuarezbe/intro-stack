import falcon


class AuthMiddleware(object):

	def process_request(self, req, resp):
		token = req.get_header('token')

		if not token == 'token_superrrr_Seguro':
			raise falcon.HTTPUnauthorized(title="Not authorized",description="The provided token is not valid")
