import falcon
from app_logging import get_logger
from products import ProductsResource
from authentication import AuthMiddleware

logger = get_logger()

api_middlewares = [AuthMiddleware()]

api = application = falcon.API(middleware=api_middlewares)
products_resource = ProductsResource(logger = logger)
api.add_route('/productos', products_resource)