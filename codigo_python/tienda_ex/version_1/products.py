import json
import falcon

class ProductsResource(object):

    def __init__(self, logger):
        self.logger = logger

    def on_get(self, req, resp):
        products_list = [
            {"id": "product-1", "name": "producto A"},
            {"id": "product-2", "name": "producto b"},
            {"id": "product-c", "name": "producto C"}
        ]
        # Create a JSON representation of the resource
        resp.body = json.dumps(products_list, ensure_ascii=False)
        resp.content_type = falcon.MEDIA_JSON
        self.logger.info("Sending HTTP GET response with {}".format( products_list ))
        resp.status = falcon.HTTP_200