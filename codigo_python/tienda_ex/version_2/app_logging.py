from logging.config import dictConfig
from logging.handlers import MemoryHandler
import logging
import os


LOGGING_LEVELS = {
    'DEBUG': logging.DEBUG,
    'INFO': logging.INFO,
    'WARNING': logging.WARNING,
    'ERROR': logging.ERROR,
    'CRITICAL': logging.CRITICAL
}

global_logger_name = 'tienda_ex'


def get_logger(name=global_logger_name , logger_config= None , filename= None ):
    """
    :param name: string that contains name of the logger. Logger name should
                 match the logger name in configuration if logger_config parameter is provided.

    :param logger_config: dictionary that contains all the logger config. If not provided
                          logger will be configured with base DirectTV Logging Config.
    :param filename
    :return: Logger instance configured with the provided information
    """
    global global_logger_name
    global_logger_name = name
    environment_logging_level = os.environ.get('LOG_LEVEL', 'INFO').upper()
    level = LOGGING_LEVELS.get(environment_logging_level)

    if not logger_config:
        logger_config = {
            'version': 1,
            'formatters': {
                name: {
                    'format': '[%(levelname)s] - %(name)s - %(filename)s - %(funcName)s : %(message)s',
                }
            },
            'handlers': {
                'console': {
                    'level': level,
                    'class': 'logging.StreamHandler',
                    'formatter': name
                }
            },
            'loggers': {
                name: {
                    'level': level,
                    'handlers': ['console'],
                    'propagate': False,
                },
            },
            'disable_existing_loggers': False
        }

        if filename:
            file_handler_name = 'file_handler'
            file_handler_config = {
                file_handler_name: {
                    'class': 'logging.FileHandler',
                    'level': level,
                    'formatter': name,
                    'filename': filename
                }
            }
            logger_config['handlers'].update(file_handler_config)
            logger_config['loggers'][name]['handlers'].append(file_handler_name)

    dictConfig(logger_config)
    return logging.getLogger(name)

def get_app_logger_name():
    """
    Return the logger instance that have been instantiated by this module
    """
    global global_logger_name
    return global_logger_name