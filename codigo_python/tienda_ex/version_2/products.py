import json
import falcon

class ProductsResource(object):

    def __init__(self, logger):
        self.logger = logger

    def on_get(self, req, resp):
        products_list = [
            {"id": "product-1", "name": "producto A"},
            {"id": "product-2", "name": "producto b"},
            {"id": "product-c", "name": "producto C"}
        ]
        # Create a JSON representation of the resource
        resp.body = json.dumps(products_list, ensure_ascii=False)
        resp.content_type = falcon.MEDIA_JSON
        self.logger.info("Sending HTTP GET response with {}".format( products_list ))
        resp.status = falcon.HTTP_200


    def on_put(self, req, resp):
        # check https://falcon.readthedocs.io/en/stable/api/request_and_response.html#id1
        new_product = json.loads( req.stream.read().decode("utf-8") )
        self.logger.info("A new product {}".format( new_product ))
        # Create a JSON representation of the resource
        msg = {"msg":"A new product has been added"}
        resp.body = json.dumps(msg, ensure_ascii=False)
        resp.content_type = falcon.MEDIA_JSON
        resp.status = falcon.HTTP_200
