import falcon
from app_logging import get_logger
from products import ProductsResource

logger = get_logger()

api = application = falcon.API()
products_resource = ProductsResource(logger = logger)
api.add_route('/productos', products_resource)