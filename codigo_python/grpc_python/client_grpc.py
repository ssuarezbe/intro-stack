import random

import grpc

import products_pb2_grpc
import products_pb2



def get_product(stub):
	product_id = products_pb2.ProductIdentifier(id='product-a')
	product = stub.GetProduct( product_id )
	print(" Product: id=>{} name=>{} value=>{}".format(product.id, product.name, product.value))



def run():
    # NOTE(gRPC Python Team): .close() is possible on a channel and should be
    # used in circumstances in which the with statement does not fit the needs
    # of the code.
    with grpc.insecure_channel('localhost:50051') as channel:
        stub = products_pb2_grpc.ProductsStub(channel)
        print("-------------- GetProduct --------------")
        get_product(stub)


if __name__ == '__main__':
    run()