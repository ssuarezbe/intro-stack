from concurrent import futures
import time
import logging
import os

import grpc

import products_pb2_grpc
import products_pb2

from datasources import DbDatasource


_ONE_DAY_IN_SECONDS = 60 * 60 * 24
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()

def emulate_db_get( product_identifier ):
    product_id = product_identifier.id
    products_db = {
        "product-a": {"id": "product-a", "name": "producto A", "value": 12.30},
        "product-b": {"id": "product-b", "name": "producto b", "value": 5},
        "product-c": {"id": "product-c", "name": "producto C", "value": 0.52}
    }
    product_dict = products_db.get(product_id, None)
    if product_dict is None:
        logger.warning("Can not found any product with the id {}".format(product_id))
        return None
    else:
        logger.info("Product found {}".format( product_dict ))
        return product_dict


def db_get( product_identifier, datasource ):
    product_id = product_identifier.id
    product_dict = datasource.get_product( product_id )
    if product_dict is None:
        logger.warning("Can not found any product with the id {}".format(product_id))
        return None
    else:
        logger.info("Product found {}".format( product_dict ))
        return product_dict


class ProductsServicer(products_pb2_grpc.ProductsServicer):


    def __init__(self):
        # This constructor will have a DBDatasource instance to have access to the DB
        self.datasource = DbDatasource()
        super().__init__()

    def GetProduct(self, request, context):
        # product = emulate_db_get(request)
        product = db_get(request, self.datasource)
        if product is None:
            return products_pb2.Product(id="NA", name="NA", value=0)
        else:
            return products_pb2.Product(**product)




def serve():
    num_workers = 3
    server_port = os.environ.get('rpc_server_port','50051')
    server_host = os.environ.get('rpc_server_host','[::]')
    server_address = '{}:{}'.format(server_host, server_port)
    logger.info("Launching server with worker={} on address={}".format(num_workers, server_address))
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=3))
    products_pb2_grpc.add_ProductsServicer_to_server(
        ProductsServicer(), server)
    server.add_insecure_port(server_address)
    server.start()
    try:
        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)


if __name__ == '__main__':
    serve()
