PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE product (id text primary key, name text, value real);
INSERT INTO "product" VALUES('product-a','producto A',12.3);
INSERT INTO "product" VALUES('product-b','producto B',5.0);
INSERT INTO "product" VALUES('product-c','producto C',0.52);
INSERT INTO "product" VALUES('product-d','producto D',10.0);
INSERT INTO "product" VALUES('product-e','producto E',7.5);
COMMIT;
