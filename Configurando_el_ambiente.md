# Intro

**NOTA:** Las instrucciones que a continuacion se listan estan dirijidas a OS Linux. Si se esta usando un OS diferente es posible que sean necesarios algunos ajustes. En caso de tener que realizar ajsutes a los pasos, por favor informar al creador de este manual para actualizar la documentacion.

# Paso 1 : Instalar el gestor de paquetes pip

## Linux

Por defecto linux ya tiene instalado una version de python3. Pero, es necesario instalalar el pip de forma manual

```
sudo apt-get update
sudo apt-get install python3-pip
```

## Windows

Para hacer la instalacion de paquetes mas sencilla sobre windows usaremos la herramienta [chocolate package manager](https://chocolatey.org/).

Por lo tanto, antes de continuar con los demas commandos se debe verificar que alla [instalado chocolate](https://chocolatey.org/docs/installation) correctamente.

Ya teniendo [chocolate](https://chocolatey.org/docs/commandslist) installado, pasemos a installar [python3](https://chocolatey.org/packages/python) y [pip](https://chocolatey.org/packages/pip)

```
choco install python
https://chocolatey.org/packages/pip
```

# Paso 2: Instalar virtualenv y virtualenvwrapper

Se presume que en este punto el gestion de paquetes python [pip](https://pypi.org/project/pip/) es capaz de instalar los paquetes de forma sencilla tanto en sistemas Linux como en sistemas Windows. Por lo cual solo se colocara una unica lista de comandos usando [pip](https://pypi.org/project/pip/).

Primero debemos instalar el [virtualenv](https://rukbottoland.com/blog/tutorial-de-python-virtualenv/) que nos permitira crear ambientes de desarrollo python diferentes en una misma maquina.

```
pip install virtualenv
```

Despues de tener instalado el **virtualenv** pasaremos a instalar el [virtualenvwrapper](https://virtualenvwrapper.readthedocs.io/en/latest/install.html) que nos permitira gestionar los ambientes de desarrollo de forma mas sencilla.

## Linux

Instala el paquete:

```
pip install virtualenvwrapper
```

[Configura la variables de entorno](https://virtualenvwrapper.readthedocs.io/en/latest/install.html#shell-startup-file) para que puedas usar la cli desde cualquier consola windows.

```
export WORKON_HOME=$HOME/.virtualenvs
export PROJECT_HOME=$HOME/Devel
source /usr/local/bin/virtualenvwrapper.sh
```

## Windows

Instala el parquete:

```
pip install virtualenvwrapper-win
```

[Configura la variables de entorno](https://virtualenvwrapper.readthedocs.io/en/latest/install.html#windows-command-prompt) para que puedas usar la cli desde cualquier consola windows.

```
export WORKON_HOME=$HOME/.virtualenvs
export MSYS_HOME=C:\msys\1.0
source /usr/local/bin/virtualenvwrapper.sh
```

# Paso 3: Probando todo

Vamos a crear un [virtuaenv](https://virtualenv.pypa.io/en/stable/) llamado **ambiente_trabajo**, el cual usara una version the `python >= 3.5`

```
mkvirtualenv --python=python3 ambiente_trabajo
```

Recuerda el commando enterior usa la variable de entorno `$python3`, si en tu sistema operativo a usar la consola y llamar `python3` no sucede nada, intenta solo usar `python` como se muestra continuacion"

```
mkvirtualenv --python=python ambiente_trabajo
```

Solo asegurate que la version de python sea `>= 3.5`, por ejemplo, al ejectuar en una consola:

```
$ python --version
Python 3.5.3
```


Continua con el siguiente paso [Iniciando con Falcon](Iniciando_con_Falcon.md)
