# Iniciando con Falcon Web API Framework

[Falcon](https://falconframework.org/) es un framework python bastante liviano, rapido y sencillo que permite crear [Microservicios](https://microservices.io/patterns/) de forma rapida.

En esta seccion no se hablara de como desplegarlo un microservicio, pero si se abordara de forma superficial el como poder probar un microserivicio creado usando [falcon](https://falconframework.org/) en un ambiente local.


El **paso 1** es instalar el framework. Para esto primero creamos un ambiente de trabajo usando [virtualenvwrapper](https://virtualenvwrapper.readthedocs.io/en/latest/install.html#quick-start) como fue descrito en [la seccion anterior](Configurando_el_ambiente.md).

```
mkvirtualenv --python=python3 falcon_test_proj
```

**Paso 2** [instalar falcon](https://falcon.readthedocs.io/en/stable/user/install.html#pypy)


```
pip install falcon
```

# Creando un REST endpoint

**Paso 1** crear la carpeta que contendra el codigo fuente del proyecto. La llamaremos **tienda_ex**.
Usando una consola podemos crear la carpeta usando el commando `mkdir tiend_ex`.

**Es importante tener en cuenta** el que para poder ir demostrado por etapas la construccion de un REST API usando falcon, se podran encontrar diferentes sub-carpetas con los nombres *version_X*. Por lo tanto, durante el manual se hara referencia al codigo presente en estas carpetas llamadas *version_X* y estas siempre podran ser consultadas.

**Paso 2** crear el modulo python principal. Lo llamaremos `app.py` y lo colaremos dentro de la carpeta `tienda_ex`. Si eres usuario linux puedes hacerlo por consola usando `touch tienda/app.py`.

**Paso 3** crear el modulo python `__init__.py` para permitir que el proyecto se pueda usar como una [paquete python](https://stackoverflow.com/questions/448271/what-is-init-py-for). Este archivo debe estar en la misma carpeta que el `app.py` y las mismo nivel. Es decir, despues de crear el `touch __init_.py` y al listar los archivos de la carpeta `tienda_ex` obtendremos:

```
$ ls tienda_ex
app.py  __init__.py
```

**Paso 4** empezar el desarrollo de nuestro microservicio. Habre la carpeta `tienda_ex` con tu IDE python preferido. Puedes usar [Pycharm](https://www.jetbrains.com/pycharm/), [Visual Studio](https://code.visualstudio.com/docs/python/python-tutorial), [Sublime Text](https://www.sublimetext.com/), entre otros


## Desarrollando nuestro microservicio

Crearemos un recurso HTTP `/productos` que soportara 2 metodos:

1. **GET** Listar los productos
2. **PUT** Agregar un nuevo producto (emulado)


## Creando el REST endpoint para GET
[Falcon REST GET](#falcon-rest-get)

Primero creamos un modulo python llamada `products.py`, puedes ver el codigo fuente [siguiente este link](codigo_python/tienda_ex/version_1/products.py)

Algunos aspecto importantes a resaltar de este modulo es la constructora de la clase que se puede indentificar por el metodo `__init__()`. Esto quiere decir que tendremos una clase llamada `ProductsResource` con una constructora que recibe una parametro `logger`.

```
class ProductsResource(object):

    def __init__(self, logger):
        self.logger = logger
```

Despues del metodo constructor `__init__()`, se podra encontrar el metodo responsable de manejar las peticiones GET. como se aclara en el [manual de falcon](https://falcon.readthedocs.io/en/stable/user/tutorial.html#creating-resources), el framework usa *duck-typing*. Asi que para poder manejar peticiones GET solo es necesario agregar un metodo ` def on_get(self, req, resp)` que se encarge de manejar ese tipo de peticiones. 

```python
def on_get(self, req, resp):
    products_list = [
        {"id": "product-1", "name": "producto A"},
        {"id": "product-2", "name": "producto b"},
        {"id": "product-c", "name": "producto C"}
    ]
    # Create a JSON representation of the resource
    resp.body = json.dumps(products_list, ensure_ascii=False)
    resp.content_type = falcon.MEDIA_JSON
    self.logger.info("Sending HTTP GET response with {}".format( products_list ))
    resp.status = falcon.HTTP_200
```

Ya se fue creado el Recurso HTTP y agregado el GET handler-method, necesitamos decirle a nuestro *api* la URL del recurso y sun clase. Por lo tanto iremos al modulo [app.py](codigo_python/tienda_ex/version_1/app.py) y agregaramos la logica para lograr esto:

```python
api = application = falcon.API()
products_resource = ProductsResource(logger = logger)
api.add_route('/productos', products_resource)
```

Una ves hecho esto, procederemos a **desplegar el API en ambiente local**. Para esto usaremos [gunicorn](https://gunicorn.org/).
Lo primero sera instalar gunicorn: `pip install gunicorn`

Despues de haberlo instalado, desplegaramos el app. Para hacer esto debemos estar ubicados dentro de la carpeta `tienda_ex` y asi evitar problemas de acceso a archivos con rutas relativas.

```
gunicorn -w 1 -b 0.0.0.0:5000 app:api
```

Los parametros usados son:

* `w 1` para indicar que solo se usara un worker
* `-b 0.0.0.0:5000` para indicar que puede accedido desde afuera usando el puerto *5000*
* `app:api` para indicar que se cargar el modulo `app.py` y usar la variable `api` dentro del modulo para inicar el servidor

Para probar que si funcionando use Postmand o curl. A continuacion se muestra el ejemplo usando curl:

```
curl -X GET http://127.0.0.1:5000/productos
```

## Creando el REST endpoint para PUT

Para agregar el metodo PUT procederemos a editar el `product.py` y agregaremos el metodo `on_put()`. 

```python
def on_put(self, req, resp):
    # check https://falcon.readthedocs.io/en/stable/api/request_and_response.html#id1
    new_product = json.loads( resp.context['product'] )
    self.logger.info("A new product {}".format( new_product ))
    # Create a JSON representation of the resource
    msg = {"msg":"A new product has been added"}
    resp.body = json.dumps(msg, ensure_ascii=False)
    resp.content_type = falcon.MEDIA_JSON
    resp.status = falcon.HTTP_200
```

Para el modulo completo con los metodos GET y PUT abre el link [products.py](codigo_python/tienda_ex/version_2/products.py).

Debido a que en el [paso anterior](#falcon-rest-get) ya se agregago el recurso `products_resource`, no necesitamos hacer modificacion de codigo adicional.

Asi que, probemos el metodo PUT.

**NOTA:**Debemos estar ubicados dentro de la carpeta donde esta ubicado el modula `app.py` a usar.
```
gunicorn -w 1 -b 0.0.0.0:5000 app:api
```

```
curl -X PUT -H "Content-Type: application/json" --data '{"id":"Nuevo-product","name":"Este es un nuevo producto"}' http://127.0.0.1:5000/productos
```


# Agregando un middleware para autenticacion

Falcon permite usar [middlewares](https://falcon.readthedocs.io/en/stable/api/middleware.html#middleware). Como se explica en la documentacion, un **middleware** es un componente que permite **ejectuar cierta logica antes** de llamar el handler-method para una peticion GET, POST, PUT y DELETE.

Por ejemplo, agregaremos un middleware que emule una atenticacion. Para esto, verificaremos que un `token` VALIDO haga parte del header del request.

Crearemos un nuevo modulo python llamado [authentication.py](codigo_python/tienda_ex/version_3/authentication.py) con el siguiente codigo:

```python
class AuthMiddleware(object):

	def process_request(self, req, resp):
		token = req.get_header('token')

		if not token == 'token_superrrr_Seguro':
			raise falcon.HTTPUnauthorized(title="Not authorized",description="The provided token is not valid")
```

Ademas, agregaremos este middlware al `api` falcon modification el [app.py](codigo_python/tienda_ex/version_3/app.py) de la siguiente forma:

```python
api_middlewares = [AuthMiddleware()]

api = application = falcon.API(middleware=api_middlewares)
products_resource = ProductsResource(logger = logger)
api.add_route('/productos', products_resource)
```

Ahora estamos listo para probar como funciona este nueva middleware. Es momento de lanzar el servidor

**NOTA:**Debemos estar ubicados dentro de la carpeta donde esta ubicado el modula `app.py` a usar.
```
gunicorn -w 1 -b 0.0.0.0:5000 app:api
```

Primero enviaremos una peticion con un `TOKEN INVALIDO`:

```
curl -X GET -H "token: token_invalid" http://127.0.0.1:5000/productos
```

Y para finalizar las pruebas enviaremos uno con el `TOKEN CORRECTO`:

```
curl -X GET -H "token: token_superrrr_Seguro" http://127.0.0.1:5000/productos
```

Las middlwares son herramientas de gran utilidad y en la [documentacion de falcon](https://falcon.readthedocs.io/en/stable/user/quickstart.html#more-features) se pueden encontrar mas ejemplos de como usar estos middlwares para hacer validaciones de otros tipos.




Con esto hemos finalizado la introduccion a Falcon y pasaremos a siguiente seccion [Iniciando con grpc.io](Iniciando_con_grpc.md)

