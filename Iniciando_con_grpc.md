
Instalar [grpc.io](https://grpc.io/docs/quickstart/python.html)

```
pip install grpcio
pip install grpcio-tools
pip install googleapis-common-protos
```

**Paso 1**, definir las structuras de datos (schema) de los mensajes que se van a intercambiar. Para esto usaremos un archivo `.proto` el cual sera usado para serializar y deserializar los mensajes. Si se desean mas detalles por favor revisar la documentacion de gRPC [Usando Protocol Buffers](https://grpc.io/docs/tutorials/basic/python.html) y [Documentacion de Protocol Buffers](https://developers.google.com/protocol-buffers/docs/overview)


```
syntax = "proto3";


package products;

service Products {
   // Create a new Product
   rpc CreateProduct(Product) returns (ProductReply) {}

   // Obtains a product given a product-id
   rpc GetProduct(ProductIdentifier) returns (Product) {}
}

message ProductIdentifier {
	// proto 3 define fields 'optional' by default
	string id = 1;
}

message Product {
	// proto 3 define fields 'optional' by default
	string id = 1;
	string name = 2;
	float value = 3;
}

message ProductReply {
	// proto 3 define fields 'optional' by default
	string response = 1;
}
```

**Paso 2**, compilar el archivo `.proto` y generar las plantillas python para cliente y servidor. Para esto debes estar en la carpeta (grpc_python)[codigo_python/grpc_python/]

```
python -m grpc_tools.protoc -I./protos --python_out=. --grpc_python_out=. ./protos/products.proto
```

El commando llamado generara 2 archivos `product_pb2.py` y `product_pb2_grpc.py`. Estos archivos los usaremos para crear el **servidor grpc**.

**Paso 3**, crearemos un `__init__.py` y un [`server_rpc.py`](codigo_python/grpc_python/server_rpc.py). Este archivo tiene 2 partes principales. La primera, donde se agrega lo logica del servicio que usara los *serializador/deserializados* creados en el **paso 2**; y la segunda donde instanciaremos un servidor grpc que use [future.concurrent](http://masnun.com/2016/03/29/python-a-quick-introduction-to-the-concurrent-futures-module.html) para permitir multihilos o multiprocesos en el servidor grpc.

```python
# =============== logica del servicio ===============
def emulate_db_get( product_identifier ):
    product_id = product_identifier.id
    products_db = {
        "product-a": {"id": "product-a", "name": "producto A", "value": 12.30},
        "product-b": {"id": "product-b", "name": "producto b", "value": 5},
        "product-c": {"id": "product-c", "name": "producto C", "value": 0.52}
    }
    product_dict = products_db.get(product_id, None)
    if product_dict is None:
        logger.warning("Can not found any product with the id {}".format(product_id))
        return None
    else:
        logger.info("Product found {}".format( product_dict ))
        return product_dict

class ProductsServicer(products_pb2_grpc.ProductsServicer):

    def __init__(self):
        # This constructor will have a DBDatasource instance to have access to the DB
        self.datasource = DbDatasource()
        super().__init__()

    def GetProduct(self, request, context):
        product = emulate_db_get(request)
        if product is None:
            return products_pb2.Product(id="NA", name="NA", value=0)
        else:
            return products_pb2.Product(**product)
```



```python
# =============== isntanciar el servidor grpc ===============
def serve():
    num_workers = 3
    server_port = os.environ.get('rpc_server_port','50051')
    server_host = os.environ.get('rpc_server_host','[::]')
    server_address = '{}:{}'.format(server_host, server_port)
    logger.info("Launching server with worker={} on address={}".format(num_workers, server_address))
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=3))
    products_pb2_grpc.add_ProductsServicer_to_server(
        ProductsServicer(), server)
    server.add_insecure_port(server_address)
    server.start()
    try:
        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)

if __name__ == '__main__':
    serve()
```

**Paso 4**, crearemos un [client_grpc.py](codigo_python/grpc_python/client_grpc.py) para testear el servidor.

```python
def get_product(stub):
	product_id = products_pb2.ProductIdentifier(id='product-a')
	product = stub.GetProduct( product_id )
	print(" Product: id=>{} name=>{} value=>{}".format(product.id, product.name, product.value))

def run():
    # NOTE(gRPC Python Team): .close() is possible on a channel and should be
    # used in circumstances in which the with statement does not fit the needs
    # of the code.
    with grpc.insecure_channel('localhost:50051') as channel:
        stub = products_pb2_grpc.ProductsStub(channel)
        print("-------------- GetProduct --------------")
        get_product(stub)

if __name__ == '__main__':
    run()
```

**Paso 5**, probar que funcionen tanto servidor como cliente. 

```
# iniciar servidor
python server_rpc.py 
```

```
# iniciar cliente
python client_grpc.py
```

Y si todo funcionar correctamente, al finilizar la ejecucion de `python client_grpc.py` debemos ver:

```
-------------- GetProduct --------------
 Product: id=>product-a name=>producto A value=>12.300000190734863
```

**Es importante aclarar** que el servicio listado anterior usa la funcion `emulate_db_get(request)` para emular como podria funcionar una busqueda sobre una BD. Pero el objetivo real es integrarlo con una BD. Por eso seguiremos con la integracion de una BD a servidor grpc

# Integrando SQLite con grpc

**Paso 1**, crearemos un clase que contenga toda la logica de conexion a la BD y tambien mantenga el estado de conexion a la BD. Es importante aclarar que buscamos que esta clase sea *thread-safe*, por lo cual el estado de conexion hace parte de la clase. Continuando, crearemos un modulo [`datasources.py`](codigo_python/grpc_python/datasources.py) el cual usara [sqlalchemy.core](https://docs.sqlalchemy.org/en/latest/core/engines.html) para manejar las conexiones a la BD.

**NOTE:**Para ver archivo SQLite de la BD da click [products.db](codigo_python/grpc_python/sqlite_db/products.db) o [products.sql](codigo_python/grpc_python/sqlite_db/products.sql)

```python
class DbDatasource(object):

    def __init__(self):
        self.db_engine = create_engine('sqlite:///sqlite_db/products.db', echo=True)

    def get_product(self, product_id):
        sql_str = "select * from product where id = '{}'"
        with self.db_engine.begin() as conn:
            row = conn.execute( sql_str.format(product_id) ).fetchone()
            return row
```

**Paso 2**, agregar la logica al [`server_rpc.py`](codigo_python/grpc_python/server_rpc.py) para que use el nuevo datasource.

```python
def db_get( product_identifier, datasource ):
    product_id = product_identifier.id
    product_dict = datasource.get_product( product_id )
    if product_dict is None:
        logger.warning("Can not found any product with the id {}".format(product_id))
        return None
    else:
        logger.info("Product found {}".format( product_dict ))
        return product_dict

class ProductsServicer(products_pb2_grpc.ProductsServicer):

    def __init__(self):
        # This constructor will have a DBDatasource instance to have access to the DB
        self.datasource = DbDatasource()
        super().__init__()

    def GetProduct(self, request, context):
        product = db_get(request, self.datasource)
        if product is None:
            return products_pb2.Product(id="NA", name="NA", value=0)
        else:
            return products_pb2.Product(**product)
```

Y ahora es momento de probar:

```
# iniciar servidor
python server_rpc.py 
```

```
# iniciar cliente
python client_grpc.py
```

Y si todo funcionar correctamente, al finilizar la ejecucion de `python client_grpc.py` debemos ver:

```
-------------- GetProduct --------------
 Product: id=>product-a name=>producto A value=>12.300000190734863
```

# Material Adicional relacionado con grpc

* [Is grpc production ready - Hacker news](https://news.ycombinator.com/item?id=14822294)

* [Using grpc in Production](https://blog.bugsnag.com/using-grpc-in-production/)

* [Herramientas para probar grpc](https://blog.bugsnag.com/using-grpc-in-production/)